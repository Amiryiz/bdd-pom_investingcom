package com.investingCom.pages;

import com.investingCom.base.Base;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchResultPage {

    //constructor
    public SearchResultPage() {

        PageFactory.initElements(Base.driver,this);

    }

    @FindBy(xpath="//div[@class='noResults']//p[@class='lighterGrayFont'][contains(text(),'No results matched your search')]")
    public static WebElement massageNoResults;

    @FindBy(xpath="//div[@class='js-section-wrapper searchSection allSection']//div[@class='resultsSum'][contains(text(),'Results:')]")
    public static WebElement numOfSearchResults;

    @FindBy(xpath="//div[@class='js-show-all-btn showAll']")
    public static WebElement showAllLink;

    @FindBy(xpath="//div[@class='js-section-wrapper searchSection allSection']//a[1]//span[3]")
    public static WebElement quotesThirdCol_CompanyName;

}
