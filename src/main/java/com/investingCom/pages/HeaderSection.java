package com.investingCom.pages;

import com.investingCom.base.Base;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class HeaderSection {

    public HeaderSection() {
        PageFactory.initElements(Base.driver, this);
    }


    @FindBy(xpath= "//a[@class='login bold']")
    public static WebElement loginHeaderLink;

    @FindBy(xpath = "//input[@placeholder='Search the website...']")
    public static WebElement searchBox;

    @FindBy(xpath = "//label[@class='searchGlassIcon js-magnifying-glass-icon']")
    public static WebElement searchGlassIcon;

    @FindBy(className = "register bold")
    public static WebElement registerHeaderLink;

    @FindBy(className = "topBarAlertsIcon")
    public static WebElement alertsHeaderIcon;

    @FindBy(className = "topBarPortfolioIcon")
    public static WebElement portfolioHeaderIcon;

    @FindBy(className = "topBarWorldMarketsIcon")
    public static WebElement worldMarketsHeaderIcon;

    @FindBy(className = "ceFlags USA middle inlineblock")
    public static WebElement flagsUSAHeaderIcon;

    public static void clickLoginHeader() {

        loginHeaderLink.click();

    }

    public static void doSearch(String searchWord) {

        searchBox.click();
        searchBox.sendKeys(searchWord);
        searchGlassIcon.click();

    }


}
