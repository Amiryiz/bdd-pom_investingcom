package com.investingCom.pages;

import com.investingCom.base.Base;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    //constructor
    public LoginPage() {

        PageFactory.initElements(Base.driver,this);

    }


    @FindBy(id="loginFormUser_email")
    public static WebElement emailField;

    @FindBy(id="loginForm_password")
    public static WebElement passwordField;

    @FindBy(xpath="//a[@class='newButton orange'][contains(text(),'Sign In')]")
    public static WebElement loginButton;


    public static void doLogin (String username,String password) {

        emailField.sendKeys(username);

        passwordField.sendKeys(password);

        loginButton.click();

    }




}
