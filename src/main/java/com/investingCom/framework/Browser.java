package com.investingCom.framework;

import com.investingCom.base.Base;

import org.apache.log4j.Logger;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Browser {

    public static Logger log = Logger.getLogger(Browser.class);
    public static String browserType;
    public static String url;

    public static String getBrowserTypeExcel() throws IOException {

        // Read the browser data from config.xlsx

        File src = new File("src\\test\\resources\\ConfigurationFile\\config.xlsx");
        FileInputStream fis = new FileInputStream(src);
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet configSheet = wb.getSheetAt(0);
        browserType = configSheet.getRow(1).getCell(1).getStringCellValue();
        url = configSheet.getRow(0).getCell(1).getStringCellValue();
        return browserType;

    }

    public static WebDriver startBrowser()  {

        try {
                browserType = Browser.getBrowserTypeExcel();
        }

        catch (Exception IOException) {

        }

        log.info("Selected Browser is: "+browserType);

        // WebDriverManager - no needs "Drivers" folder
        switch (browserType) {

            case "chrome":
                WebDriverManager.chromedriver().setup();
                Base.driver = new ChromeDriver();
                log.info("Chrome Browser is Started" + Base.driver.hashCode());
                return Base.driver;

            case "ie":
                WebDriverManager.iedriver().setup();
                Base.driver = new InternetExplorerDriver();
                log.info("Internet Explorer Browser is Started" + Base.driver.hashCode());
                return Base.driver;

            case "opera":
                WebDriverManager.operadriver().setup();
                Base.driver = new OperaDriver();
                log.info("Opera Browser is Started" + Base.driver.hashCode());
                return Base.driver;

            default:
                WebDriverManager.firefoxdriver().setup();
                Base.driver = new FirefoxDriver();
                log.info("Firefox Browser is Started By Default" + Base.driver.hashCode());
                return Base.driver;
        }
    }

    public static void maximize() {

        Base.driver.manage().window().maximize();
    }

    public static void setImpWait() {

        Base.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }


}
