Feature: Search functionality scenarios

  @Search @One
  Scenario: Verify whether the User is able to search for a valid company stock price
    Given I launch the application investing com
    When I search for a stock "BYND"
    Then I should see by search status "foundResult" the full company name "Beyond Meat Inc" in the search results

  @Search @Two
  Scenario: Verify whether the Guest User is informed when the symbol of the company not found any result
    Given I launch the application investing com
    When I search for a stock "iioo"
    Then I should see by search status "noResults" the full company name "Apple Inc" in the search results