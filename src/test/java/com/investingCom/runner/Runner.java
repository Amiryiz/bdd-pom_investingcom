package com.investingCom.runner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
//must be in resource folder
@CucumberOptions(features = {"classpath:Search.feature",
                             "classpath:Login.feature"},
                 glue = {"classpath:com.investingCom.stepdef"},
                 tags = {"@Search", "@Two"},
                 plugin = {"html:target/cucmber_html_report"}
                )

public class Runner {

}
