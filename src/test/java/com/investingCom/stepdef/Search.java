package com.investingCom.stepdef;

import com.investingCom.base.Base;
import com.investingCom.pages.HeaderSection;
import com.investingCom.pages.SearchResultPage;
import io.cucumber.java.en.*;
import org.junit.Assert;
import org.openqa.selenium.By;

public class Search {

    HeaderSection headerSection = new HeaderSection();
    SearchResultPage searchResultPage = new SearchResultPage();

    @When("^I search for a stock \"([^\"]*)\"$")
    public void i_search_for_stock(String stockSymbol)  {

        HeaderSection.doSearch(stockSymbol);

    }

    @Then("^I should see by search status \"([^\"]*)\" the full company name \"([^\"]*)\" in the search results$")
    public void i_should_see_the_full_company_name_in_the_search_results(String expectedSearchStatus, String expectedCompanyFullName){

        String actualSearchStatus = null;
        String noResultText;

        try{
            noResultText = SearchResultPage.massageNoResults.getText();
            if (noResultText.equalsIgnoreCase("No results matched your search"))
                actualSearchStatus = "noResults";

        }

        catch (Exception e) {
            if (expectedCompanyFullName.equalsIgnoreCase(SearchResultPage.quotesThirdCol_CompanyName.getText()))
                actualSearchStatus = "foundResult";
            else
                actualSearchStatus = "notFoundResult";

        }


        if (expectedSearchStatus.equalsIgnoreCase(actualSearchStatus)) {
            //Test case will pass
        }
        else
            Assert.fail("Search test has failed");

        Base.driver.quit();

    }
}
