package com.investingCom.stepdef;

import com.investingCom.base.Base;
import com.investingCom.framework.Browser;
import com.investingCom.pages.HeaderSection;
import com.investingCom.pages.LoginPage;


import io.cucumber.java.en.*;
import org.junit.Assert;
import org.openqa.selenium.By;


public class Login {

        HeaderSection headerSection = new HeaderSection();
        LoginPage loginPage = new LoginPage();

        @Given("^I launch the application investing com$")
        public void i_launch_the_application()  {

                Base.driver.get(Browser.url);
        }


        @And("I navigate to Login page")
        public void iNavigateToLoginpage(){

                HeaderSection.clickLoginHeader();
        }



        @When("^I enter Username as \"([^\"]*)\" and password as \"([^\"]*)\" into the fields$")
        public void iEnterUsernameAndPassword(String username,String password) {

                LoginPage.doLogin(username, password);

        }

        @Then("^User should login base on expected \"([^\"]*)\" status$")
        public void userShouldLoginBaseStatus(String expectedLoginStatus) {

                String actualLoginStatus = null;
                String alertText;

                try{
                        alertText = Base.driver.findElement(By.id("serverErrors")).getText();
                        if (alertText.equalsIgnoreCase("Wrong email or password. Try again."))
                                actualLoginStatus = "failure";

                }

                catch (Exception e) {

                        actualLoginStatus = "success";

                }

                if (actualLoginStatus.equalsIgnoreCase(expectedLoginStatus)) {
                        //Test case will pass
                }
                else
                        Assert.fail("Login test has failed");

                Base.driver.quit();

        }


}
