package com.investingCom.stepdef;

import io.cucumber.java.en.*;

public class SearchDemo {

    @Given("^I visit the website as a Guest user$")
    public void I_visit_the_website_as_a_Guest_user(){

        System.out.println("Given");

    }

    @When("^I select the book option from the dropdown$")
    public void I_select_the_book_option_from_the_dropdown(){

        System.out.println("When");

    }

    @And("I click on search icon")
    public void iClickOnSearchIcon() {
        System.out.println("When");
    }


    @Then("I should see the book page loaded")
    public void iShouldSeeTheBookPageLoaded() {
        System.out.println("When");
    }

    @And("I should see Books at amazon as heading")
    public void iShouldSeeBooksAtAmazonAsHeading() {
        System.out.println("When");
    }

    @But("i should not see the other category products")
    public void iShouldNotSeeTheOtherCategoryProducts() {
        System.out.println("When");
    }

    @When("I select the baby option from the dropdown")
    public void iSelectTheBabyOptionFromTheDropdown() {
        System.out.println("When");
    }

    @Then("I should see the baby page loaded")
    public void iShouldSeeTheBabyPageLoaded() {
        System.out.println("When");
    }

    @And("I should see The baby Store as heading")
    public void iShouldSeeTheBabyStoreAsHeading() {
        System.out.println("When");
    }
}
