package com.investingCom.stepdef;

import com.investingCom.base.Base;
import com.investingCom.framework.Browser;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Hooks {
    private static Logger log = Logger.getLogger(Hooks.class);

    @Before
    public void setUp (Scenario scenario) {
        PropertyConfigurator.configure("src\\test\\resources\\ConfigurationFile\\log4j.properties");
        log.info("Scenario Started: "+scenario.getName());
        Browser.startBrowser();
        Browser.maximize();
        Browser.setImpWait();
    }

    @After
    public void closeBrowser(Scenario scenario) {

        log.info("Scenario Completed: "+scenario.getName());
        log.info("Scenario Status is: "+scenario.getStatus());
        Base.driver.quit();

    }


}
