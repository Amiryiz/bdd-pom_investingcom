$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("classpath:Search.feature");
formatter.feature({
  "name": "Search functionality scenarios",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Verify whether the Guest User is informed when the symbol of the company not found any result",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Search"
    },
    {
      "name": "@Two"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I launch the application investing com",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.i_launch_the_application()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I search for a stock \"iioo\"",
  "keyword": "When "
});
formatter.match({
  "location": "Search.i_search_for_stock(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see by search status \"noResults\" the full company name \"Apple Inc\" in the search results",
  "keyword": "Then "
});
formatter.match({
  "location": "Search.i_should_see_the_full_company_name_in_the_search_results(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});