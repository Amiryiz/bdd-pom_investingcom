Feature: Login functionality scenarios to investing com site

  @Login @One
  Scenario Outline: User should only be able to login with valid credential
    Given I launch the application investing com
    And I navigate to Login page

    When I enter Username as "<username>" and password as "<password>" into the fields
    Then User should login base on expected "<loginstatus>" status

    Examples:
    |username          |password|loginstatus|
    |amiryiz@yahoo.com |123456ab|success    |
    |eli444fd@gmail.com|test777 |failure    |
    |moshe707@gmail.com|testAAA |failure    |

